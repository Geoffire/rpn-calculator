#-------------------------------------------------
#
# Project created by QtCreator 2014-02-13T23:29:09
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qt_calc
TEMPLATE = app


SOURCES += main.cpp\
    MixedNumber.cpp \
    Operator.cpp \
    button.cpp \
    Fraction.cpp \
    GUI.cpp \
    calculator.cpp

HEADERS  += \
    MixedNumber.h \
    Fraction.h \
    Operator.h \
    include.h \
    myQueue.h \
    myStack.h \
    linkedList.h \
    node.h \
    Stack_LL.h \
    voidData.h \
    Queue_LL.h \
    button.h \
    GUI.h \
    calculator.h

FORMS    +=
