#ifndef STACK_LL_H
#define STACK_LL_H

#include <sstream>
#include "linkedList.h"

template<typename T = char>
class Stack :
		private linkedList<T>
{
public:
	Stack(int s = 5);
	~Stack();
	Stack(const Stack<T> &other);
	Stack<T>& operator=(const Stack<T> &other);

	void push(const T &d);
	T pop();	//warning: IF T is a char* the user must delete what is returned
	Stack<T>& operator<<(const T &d);
	Stack<T>& operator>>(T &d);
	void swap(Stack<T> &other);
	void clear();
	void resize(int s);

	bool full()const;
	bool empty()const;
	int size()const;
	const T& top()const;  //warning: if T is char* be careful with this one

	template<typename U>
	friend std::ostream& operator<<(std::ostream& out, const Stack<U> &theStack);
	template<typename U>
	friend std::istream& operator>>(std::istream& in, Stack<U> &theStack);

private:
	int capacity, count;

	void copy(const Stack<T> &other);
	void nukem();
	void printNextFirst(std::ostream& out, int i, const node<T>* walker)const;
};


template<typename T>
Stack<T>::Stack(int s)
{
	if (s < 1)
		throw BAD_SIZE;
	capacity = s;
	count = 0;
}

template<typename T>
Stack<T>::~Stack()
{
	nukem();
}

template<typename T>
Stack<T>::Stack(const Stack<T> &other)
{
	copy(other);
}

template<typename T>
Stack<T>& Stack<T>::operator=(const Stack<T> &other)
{
	if (this != &other)
	{
		nukem();
		copy(other);
	}
	return *this;
}


template<typename T>
void Stack<T>::push(const T &d)
{
	if (full())
		throw FULL;
	linkedList<T>::insert(d);
	++count;
}

template<typename T>
T Stack<T>::pop()
{
	if (empty())
		throw EMPTY;
	--count;
	return linkedList<T>::removeHead();
}

template<typename T>
const T &Stack<T>::top() const
{
	if (empty())
		throw EMPTY;
	return linkedList<T>::anchor.nextNode()->theData();
}

template<typename T>
bool Stack<T>::full()const
{
	return (count == capacity);
}

template<typename T>
bool Stack<T>::empty()const
{
	return !count;
}

template<typename T>
int Stack<T>::size()const
{
	return count;
}

template<typename T>
void Stack<T>::swap(Stack<T> &other)
{
	swapIt(linkedList<T>::anchor.nextNode(), other.anchor.nextNode());
	swapIt(count, other.count);
	swapIt(capacity, other.capacity);
}

template<typename T>
void Stack<T>::resize(int s)
{
	if (s < 1 || s <= count)
        throw BAD_SIZE;
	capacity = s;
}

template<typename T>
void Stack<T>::clear()
{
	nukem();
}

template<typename T>
Stack<T>& Stack<T>::operator>>(T &d)
{
	d = pop();
	return *this;
}

template<typename T>
Stack<T>& Stack<T>::operator<<(const T &d)
{
	push(d);
	return *this;
}


template<typename T>
void Stack<T>::copy(const Stack<T> &other)
{
	capacity = other.capacity;
    node<T>* afterThis = &this->anchor;
    for (const node<T>* walker = other.anchor.nextNode(); walker; walker = walker->nextNode())
	{
		linkedList<T>::insertAfter(afterThis, walker->theData());
        afterThis = afterThis->nextNode();
		++count;
	}
}

template<typename T>
void Stack<T>::nukem()
{
	count = 0;
	linkedList<T>::clear();
}

template<typename T>
void Stack<T>::printNextFirst(std::ostream& out, int i, const node<T>* walker)const
{
	if(walker->nextNode())
		printNextFirst(out, i-1, walker->nextNode());
	out << *walker << std::endl;
}

template<typename U>
std::ostream& operator<<(std::ostream& out, const Stack<U> &other)
{
    if (out == std::cout)
	{
		if (other.empty())
			std::cout << "The Stack is empty" << std::endl;
		else
		{
			out << "Stack contents:" << std::endl;
			for (const node<U>* walker = other.anchor.nextNode();
							walker; walker = walker->nextNode())
				out << *walker << std::endl;
		}
	}
	else
	{
		out << "Stack Capacity : " << other.capacity << std::endl;
		other.printNextFirst(out,other.count-1,other.anchor.nextNode());
	}
	return out;
}



template<typename U>
std::istream& operator>>(std::istream& in, Stack<U> &other)
{
	other.nukem();
	U data;
    initIt(data);
    if (in == std::cin)
	{
		std::cout << "Press Enter between inputs then input Ctrl-Z to finish" << std::endl;
		while (in >> data)
			other.push(data);
		if (!in.eof())
			throw BAD_INPUT;
		in.clear();
	}
	else
	{
		std::string theLine;
		getline(in, theLine);
		theLine = theLine.substr(theLine.find(':')+2);
		other.capacity = atoi(theLine.c_str() );

		while (in >> data)
			other.push(data);
		if (in.fail() && !in.eof())
			throw BAD_INPUT;
	}
	delDy(data);
	return in;
}

#endif // STACK_LL_H
