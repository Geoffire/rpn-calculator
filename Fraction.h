#ifndef FRACTION_H
#define FRACTION_H
#include "include.h"
#include <sstream>

using namespace std;

class Fraction
{
public:
    Fraction();
	Fraction(int Numerator, int Denominator = 1);
    Fraction(const Fraction &other);
    Fraction &operator=(const Fraction &other);
    ~Fraction();

    Fraction &operator +=(const Fraction &other);
    Fraction &operator -=(const Fraction &other);
    Fraction &operator *=(const Fraction &other);
    Fraction &operator /=(const Fraction &other);
	Fraction &operator ^=(const Fraction &other);

    friend
    ostream &operator <<(ostream &out, const Fraction &other);

protected:
	void set(int num, int den);
	int gcd(int num,int den);
    void simplify(int num, int den); // Will simplify and fraction passed into it
    string show() const;

    int denominator, numerator;
};

//checks for interger overflow
void safeMultEq(int &a, const int &b);   //does a*=b
int safeMult(int a, const int &b);       //does return a*b
void safeAddEq(int &a, const int& b);    //does a+=b

#endif // FRACTION_H

