#ifndef OPERATOR_H
#define OPERATOR_H

#include "include.h"
#include "MixedNumber.h"

//check out the comments in mixed

//operator order is 0:(), 1:+-, 2:*/, 3:^
//also need the isLeftPar and isRightPar if we include them in this class.

typedef void(*opFPtr)(MixedNumber*, MixedNumber*);


class Operator
{
	public:
		Operator (char Op, int Priority, opFPtr OpFunc);
		void operate(MixedNumber* MN1, MixedNumber* MN2 = NULL);  //feel free to rename this or change how it works
		bool isLeftPar();
		bool isRightPar();
		bool twoNumsNeeded();
		std::string show();
		friend bool operator>(const Operator &lhs, const Operator &rhs);
		friend bool operator==(const Operator &lhs, const Operator &rhs);  //in priority
	private:
		char op;
		int priority;
		opFPtr opFunc;
};

Operator* getOP(char*& cPtr);

void mulOP(MixedNumber* MN1, MixedNumber* MN2);
void divOP(MixedNumber* MN1, MixedNumber* MN2);
void subOP(MixedNumber* MN1, MixedNumber* MN2);
void addOP(MixedNumber* MN1, MixedNumber* MN2);
void powOP(MixedNumber* MN1, MixedNumber* MN2);
void negOP(MixedNumber* MN1, MixedNumber*);

#endif // OPERATOR_H
