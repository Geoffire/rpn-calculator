#ifndef MYQUEUE_H
#define MYQUEUE_H

#include "Queue_LL.h"
#include "voidData.h"

typedef Queue<voidData*> VQueue;

class myQueue : private VQueue
{
    public:
		myQueue(int s = 5): VQueue(s){}  //should throw BAD_SIZE if the size is <1 or less than number of elements
		~myQueue(){}

        template<typename T>
		void enqueue(const T &data);  //should throw FULL if full

        template<typename T>
		void dequeue(T &data);  //should throw DATATYPE_MISMATCH if the type does not match, throw EMPTY if empty.

		void dequeue()   //throw EMPTY if empty.
		{
			voidData* hold = VQueue::dequeue();
			delete hold;
		}

        template<typename T>
		myQueue& operator<<(const T &data);  //should throw same as enqueue

        template<typename T>
        myQueue& operator>>(T &data);		//should throw same as dequeue

		using VQueue::clear;
		using VQueue::resize;  //should throw BAD_SIZE if the size is <1 or less than number of elements

		using VQueue::full;
		using VQueue::empty;
		using VQueue::size;


		std::string frontDataType() //returns the name() of a typeid - should throw EMPTY if empty
		{
			return VQueue::front()->type->name();
		}
		void* frontData()      //should throw EMPTY if empty
		{
			return VQueue::front()->data;
		}
		std::string backDataType()  //returns the name() of a typeid - should throw EMPTY if empty
		{
			return VQueue::back()->type->name();
		}
		void* backData()       //should throw EMPTY if empty
		{
			return VQueue::back()->data;
		}
};

template<typename T>
void myQueue::enqueue(const T &data)  //should throw FULL if full
{
	if (full())
		throw FULL;
	voidData* builder = new voidData(data);
	VQueue::enqueue(builder);
}

template<typename T>
void myQueue::dequeue(T &data)
{
	if (&typeid(data) != VQueue::front()->type)
		throw DATATYPE_MISMATCH;
	voidData* hold = VQueue::dequeue();
	data = *(T*)(hold->data);
	delete hold;
}

template<typename T>
myQueue& myQueue::operator<<(const T &data)  //should throw same as enqueue
{
	enqueue(data);
	return *this;
}

template<typename T>
myQueue& myQueue::operator>>(T &data)		//should throw same as dequeue
{
	dequeue(data);
	return *this;
}

#endif // MYQUEUE_H
