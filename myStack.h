#ifndef MYSTACK_H
#define MYSTACK_H

#include "Stack_LL.h"
#include "voidData.h"

typedef Stack<voidData*> VStack;

class myStack : private VStack
{
	public:
		myStack(int s = 5): VStack(s){} //should throw BAD_SIZE if the size is <1 or less than number of elements
		~myStack(){}

		template<typename T>
		void push(const T &data);  //should throw FULL if full

		template<typename T>
		void pop(T &data);      //should throw DATATYPE_MISMATCH if the type does not match, throw EMPTY if empty.

		void pop()           //should throw EMPTY if empty
		{
			voidData* hold = VStack::pop();
			delete hold;
		}

		template<typename T>
		myStack& operator>>(T &data);   //should throw same as pop

		template<typename T>
		myStack& operator<<(const T &data);  //should throw same as push


		using VStack::clear;
		using VStack::resize;  //should throw BAD_SIZE if the size is <1 or less than number of elements

		using VStack::full;
		using VStack::empty;
		using VStack::size;

		const std::string topDataType()const  //returns the name() of a typeid - should throw EMPTY if empty
		{
			return VStack::top()->type->name();
		}
		const void* topData()const       //should throw EMPTY if empty
		{
			return VStack::top()->data;
		}
};

template<typename T>
void myStack::push(const T &data)
{
	if (full())
		throw FULL;
	voidData* builder = new voidData(data);
	VStack::push(builder);
}

template<typename T>
void myStack::pop(T &data)
{
	if (&typeid(data) != VStack::top()->type)
		throw DATATYPE_MISMATCH;
	voidData* hold = VStack::pop();
	data = *(T*)(hold->data);
	delete hold;
}

template<typename T>
myStack& myStack::operator>>(T &data)
{
	push(data);
	return *this;
}

template<typename T>
myStack& myStack::operator<<(const T &data)
{
	pop(data);
	return *this;
}



#endif // MYSTACK_H
