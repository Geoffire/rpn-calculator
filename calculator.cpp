#include "calculator.h"

Calculator::Calculator() : last(NULL), rpnQueue(125), calcStack(125), opStack(125)
{}

Calculator::~Calculator()
{
	if (last)
		delete last;
}

void Calculator::inputCString(char* cPtr)
{
	Operator* holdOP;
	MixedNumber* holdMN;
	showRPN.clear();
	showRPN += "RPN: ";
	char *end = cPtr + strlen(cPtr);   //end of string marker
	skipSpace(cPtr);   // skip any leading space -  in include.h
	while (cPtr < end)           //go until we reach the end of the string
	{
		if((holdMN = getMN(cPtr)))          //getMN returns the resulting object and advances cPtr to the next token if a operator was next;
			inputMN(holdMN);    //put the MixedNumber in the rpnQueue
		else if ((holdOP = getOP(cPtr)))   //same as getMN only for operators
			inputOP(holdOP); //input the resulting operator
		else
			throw BAD_INPUT; //if anything not a MixedNumber or a operator it is a error.
	}
	if(rpnQueue.empty() && opStack.empty())
		throw NO_INPUT;
}

inline
void Calculator::inputMN(MixedNumber* input)
{
	showRPN += input->show();
	showRPN += " ";
	rpnQueue.enqueue(input);  //put that MixedNumber in the rpnQueue
}

inline
void Calculator::inputOP(Operator* input)
{
	Operator* hold;  //holds operators
	if (input->isRightPar())   //if the operator input is a ')'
	{
		while(!opStack.empty())  //we need to pop the operators until we find the (
		{
			opStack.pop(hold);  //pop the top into hold
			if (hold->isLeftPar())  //is it the '('
			{
				delete hold;  //yes, so delete both the '(' and the ')'
				delete input;
				return;  //and return
			}
			else
			{
				showRPN += hold->show();	// tack the operator onto the string display
				showRPN += " ";
				rpnQueue.enqueue(hold);  //if not the '(' put this operator in rpnQueue
			}
		}
		delete input;  //if the opStack is empty without finding the ( this is an error, so delete the input
		throw RIGHT_PAR_WITHOUT_LEFT;  //throw the error indication
	}
	while(1) //loop until the return is reached.
	{
		//if the input is a '(' the opStack is empty or the top holds a lower priority than the input
		if (input->isLeftPar() || opStack.empty() || *input > **(const Operator**)(opStack.topData()))  //it took me a WHILE to figure out the double pointer thing...
		{
			opStack.push(input); //load the input into the opStack
			return; //and we're done so return
		}
		else //else, the top of opStack holds a higher priority
		{
			opStack.pop(hold);
			showRPN += hold->show();
			showRPN += " ";
			rpnQueue.enqueue(hold);  //so move that operator to the rpnQueue the repeat the loop
		}
	}
}

string& Calculator::calculate()
{
	MixedNumber* MN1;   //these hold the MixedNumbers we pop off the stack.
	MixedNumber* MN2 = NULL;
	Operator* OP;
	finishRPN();		//make sure all the operators were loaded from calcStack into RPN
	while(!rpnQueue.empty())   // process all elements in the RPNQueue
	{
		if (sameType(MN1,rpnQueue))  //if the front of the rpnQueue is a MixedNumber
		{
			moveMN(calcStack,rpnQueue,MN1); //move it onto the calcStack
		}
		else   //if not then the front of RPN holds a Operator
		{
			rpnQueue.dequeue(OP);
			try
			{
				if(OP->twoNumsNeeded())
				{
					calcStack.pop(MN2);  //pop() two values from calcStack
					calcStack.pop(MN1);  //reverse numbering as it's a stack...
				}
				else
					calcStack.pop(MN1);  //pop() just one value for unary negation
			}
			catch(ERRORS e)   //there was an error thrown in the pop()'s
			{
				if (e == EMPTY)  //if the stack is empty there is an operator out of place
					throw TOO_MANY_OPERATORS;  //so indicate so
				else
					throw e;  //otherwise pass the error along
			}
			OP->operate(MN1,MN2);   //operate on these two MixedNumbers - the result is stored in MN1
			calcStack.push(MN1);  //push the result onto calcStack
			if (MN2)
			{
				delete MN2;			//throw away the other MixedNumber
				MN2 = NULL;
			}
			delete OP;
			OP = NULL;
		}
	}
	calcStack.pop(last);
	if(!calcStack.empty())
		throw TOO_MANY_NUMBERS;
	showRPN += "  Answer: ";
	showRPN += last->show();
	return showRPN;
}

void Calculator::clear()
{
	rpnQueue.clear();
	calcStack.clear();
	opStack.clear();
	showRPN.clear();
}

inline
void Calculator::finishRPN()
{
	Operator* hold = NULL;
	while(!opStack.empty())  //while opStack holds something
	{
		if ((*(Operator**)opStack.topData())->isLeftPar())  //check if a '(' was left over
			throw LEFT_PAR_WITHOUT_RIGHT;  //indicate the error
		opStack.pop(hold);
		showRPN += hold->show();
		showRPN += " ";
		rpnQueue.enqueue(hold);
	}
}

std::istream& operator>>(std::istream& in, Calculator &other)
{
	char line[257]; //make a char array
	*line = '\0';
	in.getline(line+1,255);  //get the input
	other.inputCString(line+1);   //process it
	return in;  //return the istream
}

std::ostream& operator<<(std::ostream& out, const Calculator &other)
{
	if (other.calcStack.size() == 1) //if this is asked with one thing on thes opStack we display that one thing - the result of calculations
		out << "The answer is :" << *((MixedNumber*)other.opStack.topData())<<std::endl;
	else  //else we display the rpnQueue
		out << "The RPN equivilent to your input is: "<< other.showRPN << std::endl;
	return out;  //return the ostream
}

//Helpers

//simply for ease of readability. Inline so no waste
template <typename T>
inline
bool sameType(T &setThis, myQueue& Q)  //Queue version
{
	return (typeid(setThis).name() == Q.frontDataType());  //do the types match?
}
template <typename T>
inline
bool sameType(T &setThis, myStack& S)   //Stack version
{
	return (typeid(setThis).name() == S.topDataType());
}

// these move from a stack to a queue or vise versa with known types - designed inline, the holder is so no additional stack space needed
inline
void moveOP(myQueue& dest, myStack& src, Operator* &holder)
{
	src.pop(holder);
	dest.enqueue(holder);
}
inline
void moveOP(myStack& dest, myQueue& src, Operator* &holder)
{
	src.dequeue(holder);
	dest.push(holder);
}
inline
void moveMN(myQueue& dest, myStack& src, MixedNumber* &holder)
{
	src.pop(holder);
	dest.enqueue(holder);
}
inline
void moveMN(myStack& dest, myQueue& src, MixedNumber* &holder)
{
	src.dequeue(holder);
	dest.push(holder);
}


